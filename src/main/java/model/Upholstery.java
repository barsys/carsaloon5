package model;

public enum Upholstery {
    WELUR(0),
    SKORZANA(1000),
    SKORZANO_PIKOWANA(2000);

    private int price;

    Upholstery(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}
