package model;

public enum CarBody {
    SEDAN(0),
    HATCHBACK(1000),
    COMBI(1000),
    PICK_UP(2000);

    private int price;

    CarBody(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "CarBody{" +
                "price=" + price +
                '}';
    }
}
