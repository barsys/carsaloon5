package model;

public enum Color {
    WHITE(0),
    SILVER(0),
    RED(2000),
    BLUE(2000),
    GREEN(2000);

    private int cena;

    Color(int cena) {
        this.cena = cena;
    }

    public int getCena() {
        return cena;
    }

    @Override
    public String toString() {
        return super.toString() + " cena= " + cena;
    }
}
