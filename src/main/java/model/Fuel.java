package model;

public enum Fuel {
    BENZYNA(0),
    DIESEL(10000),
    HYBRYDA(15000);

    private int price;

    Fuel(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

}
