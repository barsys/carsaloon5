package model;


public class Car {

        Fuel fuel ;
        Upholstery upholstery;
        CarBody carBody;
        Color color;


        public Fuel getFuel() {
                return fuel;
        }

        public void setFuel(Fuel fuel) {
                this.fuel = fuel;
        }

        public Upholstery getUpholstery() {
                return upholstery;
        }

        public void setUpholstery(Upholstery upholstery) {
                this.upholstery = upholstery;
        }

        public CarBody getCarBody() {
                return carBody;
        }

        public void setCarBody(CarBody carBody) {
                this.carBody = carBody;
        }

        public Color getColor() {
                return color;
        }

        public void setColor(Color color) {
                this.color = color;
        }
}
