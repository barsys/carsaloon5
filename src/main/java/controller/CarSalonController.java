package controller;

import model.*;
import view.CarSalon;
import view.CarSalonMVC;

public class CarSalonController implements CarSalonMVC.Controller {

    private CarSalonMVC.View view;
    private Car car;
    private CarSalonScanerController carSalonScanerController;

    public CarSalonController (){
        car = new Car();
        carSalonScanerController = new CarSalonScanerController();
    }

    public void attach(CarSalonMVC.View view) {
        this.view = view;
    }

    public void addColor ()
    {
        switch (carSalonScanerController.getOption())
        {
            case 0:
            {
                car.setColor(Color.BLUE);

            }
            case 1:{
                car.setColor(Color.GREEN);
            }
            case 2:{
                car.setColor(Color.RED);
            }
            case 3:
            {
                car.setColor(Color.SILVER);
            }
            case 4:
            {
                car.setColor(Color.WHITE);
            }
            default: {
                addColor();
            }

        }

    }

    public void addUpholstery ()
    {
        switch (carSalonScanerController.getOption())
        {
            case 0:
            {
                car.setUpholstery(Upholstery.SKORZANA);

            }
            case 1:{
                car.setUpholstery(Upholstery.SKORZANO_PIKOWANA);
            }
            case 2:{
                car.setUpholstery(Upholstery.WELUR);
            }

            default: {
                addUpholstery();
            }

        }

    }

    public void addCarBody ()
    {
        switch (carSalonScanerController.getOption())
        {
            case 0 :
            {
                car.setCarBody(CarBody.SEDAN);
            }

            case 1 :{
                car.setCarBody(CarBody.PICK_UP);
            }

            case 2 :{
                car.setCarBody(CarBody.HATCHBACK);
            }

            case 3 :{
                car.setCarBody(CarBody.COMBI);
            }

            default:{
                addCarBody();
            }
        }
    }

    public void addFuel ()
    {
        switch (carSalonScanerController.getOption())
        {
                case 0 :{
                    car.setFuel(Fuel.BENZYNA);
                }
                case 1: {
                    car.setFuel(Fuel.DIESEL);
                }
                case 2: {
                    car.setFuel(Fuel.HYBRYDA);
                }

                default:{
                    addFuel();
                }

        }
    }



}
