package controller;

import view.CarSalonScannerMVC;

import java.util.Scanner;

public class CarSalonScanerController implements CarSalonScannerMVC.Controller {

    public int getOption() {
        return new Scanner(System.in).nextInt();
    }

}

