package view;

public interface CarSalonScannerMVC {

    interface Controller {
        int getOption();
    }

    interface View {

    }
}

