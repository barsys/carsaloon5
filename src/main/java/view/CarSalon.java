package view;

import controller.CarSalonController;
import controller.CarSalonMainMenuControler;
import controller.CarSalonScanerController;
import model.*;

public class CarSalon implements CarSalonMVC.View, CarSalonMainMenuMVC.View,CarSalonScannerMVC.View {

    private CarSalonMVC.Controller controller;
    private CarSalonMainMenuMVC.Controller mainMenu;
    private CarSalonScannerMVC.Controller scaner;



    public CarSalon() {
        this.controller = new CarSalonController();
        this.mainMenu = new CarSalonMainMenuControler();
        this.scaner = new CarSalonScanerController();

        controller.attach(this);
        mainMenu.attach(this);
    }

    public void showMenu() {

        System.out.println("Witaj w salonie samocodowym");
        System.out.println("Co chcesz zrobić");
    }

    public void showFuelType() {

        System.out.println("Dostepne roodzaje paliwa");
        System.out.println(Fuel.DIESEL.toString());
        System.out.println(Fuel.BENZYNA.toString());
        System.out.println(Fuel.HYBRYDA.toString());


    }
    public void showUpholstery() {

        System.out.println("Dostepne rodzaje wykonczenia");
        System.out.println(Upholstery.SKORZANA.toString());
        System.out.println(Upholstery.SKORZANO_PIKOWANA.toString());
        System.out.println(Upholstery.WELUR.toString());
    }
    public void showColor() {
        System.out.println("Dostepne kolory");
        System.out.println(Color.BLUE.toString());
        System.out.println(Color.GREEN.toString());
        System.out.println(Color.RED.toString());
        System.out.println(Color.SILVER.toString());
        System.out.println(Color.WHITE.toString());

    }
    public void showCarBody() {
        System.out.println("Dostepne rodzaje zawieczenia");
        System.out.println(CarBody.COMBI);
        System.out.println(CarBody.HATCHBACK);
        System.out.println(CarBody.PICK_UP);
        System.out.println(CarBody.SEDAN);


    }
}

