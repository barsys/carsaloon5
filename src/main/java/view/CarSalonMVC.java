package view;

public interface CarSalonMVC {

    interface Controller {

        void attach(View view);


    }

    interface View {

        void showFuelType();
        void showUpholstery();
        void showColor();
        void showCarBody();

    }
}

