package view;

public interface CarSalonMainMenuMVC {

    interface Controller {

        void attach(View view);
    }

    interface View {

        void showMenu ();
    }
}
